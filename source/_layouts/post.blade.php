@extends('_layouts.master')

@section('head')
<title>{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}</title> 
    <meta name="keywords" content="{{ $page->seokeywords ? $page->seokeywords : seo("post", $page->tags, $page->categories, $page->title, "keywords") }}">
    <meta name="description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}">
    <link rel="canonical" href="{{ $page->mainUrl }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}" />
    <meta property="og:description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}" />
    <meta property="og:url" content="{{ $page->mainUrl }}/{{ $page->title }}" />
    <meta property="og:site_name" content="{{ $page->sitename }}" />
    <meta property="og:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}" />
    <meta name="twitter:title" content="{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}" />
    <meta name="twitter:site" content="{{ '@' }}{{ $page->mainUrl }}" />
    <meta name="twitter:image" content="{{ $page->sitename }}/asset/images/tech2how-thumb-2.png" />
@endsection

@section('content')
	
<div class="uk-container uk-section uk-padding-remove-bottom">
	<article class="post-list">

	    <div class="post-header">
	        <div class="post-meta-date">
	            <p>Published on <span>{{ date('j', $page->date) }}</span> {{ date('F', $page->date) }}</p>
	        </div>
	        
	        <div class="post-title">
	            <h2 class="uk-text-secondary">{{ $page->title }}</h2>
	            <div class="post-meta">
	            	@if($page->authorname)
		                <span> By </span>
		                <a href="">{{ $page->authorname }}, </a>
		            @endif
	                <span>In </span>
	                <a href="/{{ name_from_path($page->categories) }}" title="View all Posts in Category: “{{ name_from_path($page->categories) }}”">{{ name_from_path($page->categories) }}</a>
	            </div>
	        </div>
	    </div>
	    <div class="post-featured-image uk-text-center">
	        <figure>
	            <img src="{{ $page->mainUrl }}/images/{{ basename($page->image) }}">
	        </figure>
	    </div>
	    <div class="post-body">
	        <p>@yield('postContent')</p>
	    </div>
	    <div class="post-tags">
			# 
			@foreach ($page->tags as $i => $tag)
				<a href="{{ '/tag/' . name_from_path($tag) }}" title="View all Posts in tag {{ name_from_path($tag) }}">
					{{ name_from_path($tag) }}
				</a>
				@if (! $loop->last)
					,&nbsp;
				@endif
			@endforeach
		</div>
		<div class="social-share-post">
			<span>Share on : </span>
			<a href="#" class="uk-icon-link uk-margin-small-right" uk-icon="facebook"></a>
			<a href="#" class="uk-icon-link uk-margin-small-right" uk-icon="twitter"></a>
			<a href="#" class="uk-icon-link" uk-icon="google-plus"></a>
		</div>
	  	<div class="post-footer">
	  		<nav class="navigation post-navigation">
	  			<div class="nav-links" uk-grid>
	  				<div class="nav-previous uk-width-1-2 uk-text-left">
	  					@if ($next = $page->getNext())
		  					<a href="https://demo.awaikenthemes.com/blogman/unbelievable-gossip-movie-success-stories/" rel="prev">
		  						<span class="post-title">
		  							<span>&LeftArrow; {{ ucwords($next->title) }}</span>
		  						</span>
		  					</a>
		  				@endif
	  				</div>
	  				<div class="nav-next uk-width-1-2 uk-text-right">
	  					@if ($previous = $page->getPrevious())
		  					<a href="https://demo.awaikenthemes.com/blogman/an-expert-interview-about-delicious-magazines/" rel="next">
		  						<span class="post-title">
		  							<span>{{ ucwords($previous->title) }} &RightArrow;</span> 
		  						</span>
		  					</a>
	  					@endif
	  				</div>
	  			</div>
	  		</nav>
	  	</div>
	  	<div class="post-comments">
	  		@include('_partials.comments')
	  	</div>
	</article>
</div>

	

@endsection