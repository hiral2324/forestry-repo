


@include('_partials.header')

	<div class="uk-grid-collapse" uk-grid>
		<div class="uk-width-1-5@l">
	   		@include('_partials.sidebar')
	   	</div>
	    <div class="uk-width-4-5@l uk-width-1-1@m">
	        @include('_partials.nav')
	            
	            @yield('content')
	            
	        @include('_partials.footer')
	    </div>
	    
	</div>

