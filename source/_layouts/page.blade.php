@extends('_layouts.master')

@section('head')
<title>{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}</title> 
    <meta name="keywords" content="{{ $page->seokeywords ? $page->seokeywords : seo("post", $page->tags, $page->categories, $page->title, "keywords") }}">
    <meta name="description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}">

    <title>{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}</title>
    <meta name="description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}"/>
    <meta name="keywords" content="{{ $page->seokeywords ? $page->seokeywords : seo("post", $page->tags, $page->categories, $page->title, "keywords") }}"/>
    <link rel="canonical" href="{{ $page->mainUrl }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}" />
    <meta property="og:description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}" />
    <meta property="og:url" content="{{ $page->mainUrl }}/{{ $page->title }}" />
    <meta property="og:site_name" content="{{ $page->sitename }}" />
    <meta property="og:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{ $page->seodescription ? $page->seodescription : seo("post", "", "", regex($page), "description") }}" />
    <meta name="twitter:title" content="{{ $page->seotitle ? $page->seotitle : $page->title}} | {{ $page->sitename }}" />
    <meta name="twitter:site" content="{{ '@' }}{{ $page->mainUrl }}" />
    <meta name="twitter:image" content="{{ $page->sitename }}/asset/images/tech2how-thumb-2.png" />
@endsection

@section('content')
	<div class="uk-container uk-section">
        <div class="page-title uk-text-center">
            <h1>{{ ucwords($page->title) }}</h1>
        </div>
	 	<div class="uk-margin-medium" uk-grid>
	 		<div class="uk-width-1-2">
	 			@if( $page->image )
	 				<img src="{{ $page->image }}">
	 			@endif
                 
                @if( $page->ShowForm )
                     @include('_partials.contact-form')
                @endif
		 	</div>
		 	<div class="uk-width-1-2">
		 		{!! $page !!}
		 	</div>
	 	</div>
	</div>
@endsection
