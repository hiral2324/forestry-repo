@extends('_layouts.master')

@section('head')
<title>{{ seo("tag", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}</title> 
    <meta name="keywords" content="Tag, {{ seo("tag", $page->name(), "", "", "") }}, {{ seo("tag", $page->name(), "", "", "") }} tag">
    <meta name="description" content="Post under tag {{ seo("tag", $page->name(), "", "", "") }}">

    <title>{{ seo("tag", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}</title>
    <meta name="description" content="Post under tag {{ seo("tag", $page->name(), "", "", "") }}"/>
    <meta name="keywords" content="tag, {{ seo("tag", $page->name(), "", "", "") }}, {{ seo("tag", $page->name(), "", "", "") }} tag"/>
    <link rel="canonical" href="{{ $page->mainUrl }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ seo("tag", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}" />
    <meta property="og:description" content="Post under tag {{ seo("tag", $page->name(), "", "", "") }}" />
    <meta property="og:url" content="{{ $page->mainUrl }}/tag/{{ $page->name() }}" />
    <meta property="og:site_name" content="{{ $page->sitename }}/tag/{{ $page->name() }}" />
    <meta property="og:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Post under tag {{ seo("tag", $page->name(), "", "", "") }}" />
    <meta name="twitter:title" content="{{ seo("tag", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}" />
    <meta name="twitter:site" content="{{ '@' }}{{ $page->sitename }}" />
    <meta name="twitter:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
@endsection

@section('content')
	<div class="uk-container uk-section uk-padding-remove-bottom">
		<div>
			<div class="page-title uk-text-center">
                <h1>Tag: {{ $page->name() }}</h1>
            </div>
			@forelse (posts_filter($posts, $page) as $post)
				@include('_partials.components.post-preview')
        	@empty
				<p>No posts to show.</p>
			@endforelse
		</div>
	</div>
@endsection