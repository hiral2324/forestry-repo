@extends('_layouts.master')

@section('head')
<title>{{ seo("category", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}</title> 
    <meta name="keywords" content="Category, {{ seo("category", $page->name(), "", "", "") }}, {{ seo("category", $page->name(), "", "", "") }} category">
    <meta name="description" content="Post under category {{ seo("category", $page->name(), "", "", "") }}">

    <title>{{ seo("category", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}</title>
    <meta name="description" content="Post under category {{ seo("category", $page->name(), "", "", "") }}"/>
    <meta name="keywords" content="Category, {{ seo("category", $page->name(), "", "", "") }}, {{ seo("category", $page->name(), "", "", "") }} category"/>
    <link rel="canonical" href="{{ $page->mainUrl }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ seo("category", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}" />
    <meta property="og:description" content="Post under category {{ seo("category", $page->name(), "", "", "") }}" />
    <meta property="og:url" content="{{ $page->mainUrl }}/{{ $page->name() }}" />
    <meta property="og:site_name" content="{{ $page->sitename }}/{{ $page->name() }}" />
    <meta property="og:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Post under category {{ seo("category", $page->name(), "", "", "") }}" />
    <meta name="twitter:title" content="{{ seo("category", ucwords($page->name()), "", "", "") }} | {{ $page->sitename }}" />
    <meta name="twitter:site" content="{{ '@' }}{{ $page->sitename }}" />
    <meta name="twitter:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
@endsection

@section('content')
	<div class="uk-container uk-section uk-padding-remove-bottom">
		<div>
			<div class="page-title uk-text-center">
                <h1>Category: {{ $page->name() }}</h1>
            </div>
			@forelse (posts_filter_cat($posts, $page) as $post)
				@include('_partials.components.post-preview')
			@empty
				<p>No posts to show.</p>
			@endforelse
		</div>
	</div>
@endsection