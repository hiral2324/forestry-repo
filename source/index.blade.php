---
pagination:
    collection: posts
    perPage: 2
---
@extends('_layouts.master')

@section('head')
<title>{{ $settings->setting->seotitle}} | {{ $page->sitename }}</title> 
    <meta name="keywords" content="{{ $settings->setting->seokeywords }}">
    <meta name="description" content="{{ $settings->setting->seodescription }}">

    <title>{{ $settings->setting->seotitle}} | {{ $page->sitename }}</title>
    <meta name="description" content="{{ $settings->setting->seodescription }}"/>
    <meta name="keywords" content="{{ $settings->setting->seokeywords }}"/>
    <link rel="canonical" href="{{ $page->mainUrl }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $settings->setting->seotitle}} | {{ $page->sitename }}" />
    <meta property="og:description" content="{{ $settings->setting->seodescription }}" />
    <meta property="og:url" content="{{ $page->mainUrl }}" />
    <meta property="og:site_name" content="{{ $page->sitename }}" />
    <meta property="og:image" content="{{ $page->mainUrl }}/asset/images/tech2how-thumb-2.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{ $settings->setting->seodescription }}" />
    <meta name="twitter:title" content="{{ $settings->setting->seotitle}} | {{ $page->sitename }}" />
    <meta name="twitter:site" content="{{ '@' }}{{ $page->mainUrl }}" />
    <meta name="twitter:image" content="{{ $page->sitename }}/asset/images/tech2how-thumb-2.png" />
@endsection

@section('content')
    <div class="uk-container uk-section uk-padding-remove-bottom">
        @foreach ($pagination->items as $post)
            @include('_partials.components.post-preview')
        @endforeach

        <div class="pagination"> 
            @include('_partials.components.pagination')
        </div>
    </div>
@endsection