---
title: About
seotitle: About us
seokeywords: About, About us, What we do
seodescription: About us page What we do
image: /images/author-img-large.jpeg
ShowForm: false
---

### Who am I.?

This is one of the difficult question to answer. Let me introduce myself as [Sanam Patel](https://profiles.google.com/109437141952206333342), a 19 years old computer engineering student  from Ahmedabad, Gujarat, India.

I am the chief  blogger and editor of  this blog (Tech2How). It has been almost 6 years since I started using internet. In this long  way I learned and experienced so many things about Internet, Software, Web development  and  tech stuffs. Via this blog I would like to share the experience I gained. I will be writing posts on what I have leaned about computers either from books, Internet or Manually.
