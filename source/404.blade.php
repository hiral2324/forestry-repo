@extends('_layouts.master')

@section('head')
	<title>404 | Page not Found!</title>
	<meta name="keywords" content="404, Page not found">
	<meta name="description" content="Page not found, 404 page">
@endsection

@section('content')
	<div class="uk-container uk-section">
		<div class="page-title">
			<h2 class="uk-h1e">
				404
			</h2>
			<p class="uk-text-large">Page Not Found!</p>
			<p>
				<a class="uk-text-large" href="{{ $page->mainurl }}">Go to Homepage</a>
			</p>
		</div>
	</div>
@endsection
