---
title: Motorola E Launches
authorname: Ilarin
date: 2014-07-18T00:00:00.000Z
seotitle: About staticPress
seokeywords: 'Moto E, Motorola, India, Flipkart, Motorola Moto E, Moto E Price'
seodescription: After the successful launch of Moto G and then Moto X in India, motorola has been decided to launch their new moto E in Indian consumer market.
tags:
  - flipkart
  - motorola
  - works
slug: motorola-e-launches
categories: mobile
isPosts: true
image: /images/post.jpg
comments: true
---
After the successful launch of Moto G and then Moto X in India, motorola has been decided to launch their new moto E in Indian consumer market. Moto X and Moto G both are budget phone and give you what you spent (bang for the bucks). 

Right now it is exclusive to Flipkart only and it is price only at Rs. 6999/- and moto E gives full of what you spend. With this feature set provided at this price we think this will be the next success model.

## Moto E Specification

*   With the Qualcomm Snapdragon 200 with Dual-core 1.2 GHz Cortex-A7 Chipset
*   Pure Android 4.4 Kitkat OS
*   Dual Sim with Dual Standby (GSM + GSM)
*   4.3 inches of capacitive  touchscreen with Gorilla Glass 3
*   4 GB of internal memory and 1 GB of RAM and 32 GB of expandable memory
*   5 MP Primary Camera
*   WiFi, FM Radio & Bluetooth 4.0
*   1980 mAh of battery with the full day of back up

Buy Motorola Moto E directly from [Flipkart](https://www.flipkart.com/moto-e/p/itmdvuwsybgnbtha?pid=MOBDVHC6XKKPZ3GZ&affid=sanampatel "Motorola Moto E on Flipkart")