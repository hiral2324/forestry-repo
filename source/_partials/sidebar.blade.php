<div class="uk-visible@l uk-background-muted sidebar">
    <div class="uk-text-center">
        <img src="https://demo.awaikenthemes.com/blogman/wp-content/uploads/2018/03/logo.jpg" style="border-radius: 50%">
        <h3 class="uk-text-bold uk-margin-small-top uk-margin-remove-bottom">Author Name</h3>
        <small>My Personal Blog</small>
        <div>
            <a href="" class="uk-margin-small-right" uk-icon="twitter"></a>
            <a href="" class=" uk-margin-small-right" uk-icon="facebook"></a>
            <a href="" uk-icon="google-plus"></a>
        </div>
    </div>
    <hr>
    @php
        $nav_json_content = file_get_contents('./source/navigation.json');
        $arr =json_decode($nav_json_content, true);
    @endphp
    <ul class="uk-nav uk-nav-default">
        @foreach ($arr['items'] as $arrss) 
            <li class="uk-margin-small-top">
                <a class="{{ $page->_meta->url == $arrss['url'] ? 'selectedurl' : 'not' }}" href="{{ $arrss['url'] }}" id="red">
                    <span class="uk-button-text">
                        {{ $arrss['text'] }}
                    </span>
                </a>
            </li>
        @endforeach
    </ul>
    <hr>
    <div class="uk-margin">
    <form class="uk-search uk-search-default">
        <span class="uk-search-icon-flip" uk-search-icon></span>
        <input class="uk-search-input" type="search" placeholder="Search...">
    </form>
</div>
</div>