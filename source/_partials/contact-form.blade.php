<h3>
	Fill the form
</h3>
<form class="uk-margin-medium contact-form" name="contact" method="POST" netlify>
	<div uk-grid>
		<div class="uk-width-1-3">
			<div class="uk-margin">
				<label class="uk-form-label" for="name">Name*</label>
				<div class="uk-form-controls">
					<input class="uk-input" id="name" type="text">
				</div>
			</div>
		</div>
		<div class="uk-width-1-3">
			<div class="uk-margin">
				<label class="uk-form-label" for="email">Email*</label>
				<div class="uk-form-controls">
					<input class="uk-input" id="email" type="text">
				</div>
			</div>
		</div>
		<div class="uk-width-1-3">
			<div class="uk-margin">
				<label class="uk-form-label" for="subject">Subject*</label>
				<div class="uk-form-controls">
					<input class="uk-input" id="subject" type="text">
				</div>
			</div>
		</div>
	</div>
	<div uk-grid>
		<div class="uk-width-1-1">
			<div class="uk-margin">
				<label class="uk-form-label" for="name">Message*</label>
				<div class="uk-form-controls">
					<textarea class="uk-textarea" rows="5"></textarea>
				</div>
			</div>
		</div>
	</div>
	<div uk-grid>
		<div class="uk-width-1-3">
			<button class="uk-button uk-button-primary">Submit</button>
		</div>
	</div>
</form>