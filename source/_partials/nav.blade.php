
{{-- <nav class="uk-visible@m" uk-navbar>
	<div class="uk-navbar-left">
		
	</div>
	<div class="uk-navbar-right">
        @php
            $nav_json_content = file_get_contents('./source/navigation.json');
            $arr =json_decode($nav_json_content, true);
        @endphp
        <ul class="uk-navbar-nav">
            @foreach ($arr['items'] as $arrss) 
                <li class="py-2">
                    <a class="{{ $page->_meta->url == $arrss['url'] ? 'selectedurl' : 'not' }}" href="{{ $arrss['url'] }}" id="red">
                        <span class="uk-button-text">
                            {{ $arrss['text'] }}
                        </span>
                    </a>
                </li>
                <hr />
            @endforeach
        </ul>
	</div>
</nav> --}}

<header class="uk-background-muted">
    <div class="uk-container">
        <nav class="uk-background-muted uk-hidden@l" uk-navbar id="min_nav">
            <div class="uk-navbar-left">
                <ul class="uk-navbar-nav">
                    <li>
                        <a class="navbar-brand" href="https://demo.awaikenthemes.com/blogman/">
                            <img src="https://demo.awaikenthemes.com/blogman/wp-content/uploads/2018/03/logo.jpg" class="logo" alt="Blogman">
                            <span>Author Name</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="uk-navbar-right">
               <a class="uk-navbar-toggle"></a>
               <a class="uk-navbar-toggle uk-button" href="#offcanvas-slide" uk-toggle>
                  <span uk-navbar-toggle-icon></span> 
                  <span class="uk-margin-small-left">Menu</span>
               </a>
               <div id="offcanvas-slide" uk-offcanvas class="uk-hidden@m">
                  <div class="uk-offcanvas-bar">
                     <button class="uk-offcanvas-close" type="button" uk-close></button>
                    @php
                        $nav_json_content = file_get_contents('./source/navigation.json');
                        $arr =json_decode($nav_json_content, true);
                    @endphp
                    <ul class="uk-nav uk-nav-default">
                        @foreach ($arr['items'] as $arrss) 
                            <li class="uk-margin-small-top">
                                <a class="{{ $page->_meta->url == $arrss['url'] ? 'selectedurl' : 'not' }}" href="{{ $arrss['url'] }}" id="red">
                                    <span class="uk-button-text">
                                        {{ $arrss['text'] }}
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                  </div>
               </div>
            </div>
        </nav>
    </div>
</header>