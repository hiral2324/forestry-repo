<nav aria-label="..." class="pagination uk-margin-medium-top">
	@if ($previous = $pagination->previous)
		<a href="{{ $previous }}" title="Previous Page" class="btn uk-float-left">
			 <span class="uk-margin-medium-right">&larr; </span> Previous 
			 Page
		</a>
	@endif
	{{-- @foreach ($pagination->pages as $pageNumber => $path)
		<a
		href="{{ $path }}"
		title="Go to Page {{ $pageNumber }}"
		class="border border-brand-light px-3 py-2 hover:bg-gray-300 hover:text-black text-brand-dark no-underline {{ $pagination->currentPage == $pageNumber ? 'selected' : '' }}">
		{{ $pageNumber }}
	</a>
	@endforeach --}}
	@if ($next = $pagination->next)
		{{-- Next --}}
		<a
			href="{{ $next }}"
			title="Next Page"
			class="btn uk-float-right">
			Next Page <span class="uk-margin-medium-left">&rarr; </span> 
		</a>
	@endif
</nav>