@if (! $loop->first)
    <div class="article-margin"></div>
@endif
<article class="post-list {{ $loop->first ? 'first' : 'no-border' }}">

    <div class="post-header">
        <div class="post-meta-date">
            <p>Published on <span>{{ date('j', $post->date) }}</span> {{ date('F', $post->date) }}</p>
        </div>
        
        <div class="post-title">
            <h2><a href="{{ $post->getUrl() }}" title="Read more - {{ $post->title }}" class="uk-text-secondary uk-text-bold">{{ $post->title }}</a></h2>
            <div class="post-meta">
                @if($post->authorname)
                    <span> By </span>
                    <a href="">{{ $post->authorname }}</a>
                @endif
                <span>, In </span>
                <a href="/{{ name_from_path($post->categories) }}" title="View all Posts in Category: “{{ name_from_path($post->categories) }}”">{{ name_from_path($post->categories) }}</a>
            </div>
        </div>
    </div>
    <div class="post-featured-image uk-text-center" style="padding-top: 60px">
        <figure>
            <img src="{{ $post->mainUrl }}/images/{{ basename($post->image) }}">
        </figure>
    </div>
    <div class="post-body">
        <p>{{ $post->excerpt(350) }}</p>
    </div>
    <div class="post-footer">
        <a href="{{ $post->getUrl() }}" class="btn-read-more">Read More &rarr; </a>
    </div>
  
</article>
