<?php

return [
	'production' => false,
	'google_analytic_code' => 'UA-23560239-20', // GA Code added in footer
	'disqus_code' => 'ilarin-blog', // Disqus Code added in footer
	'sitename' => 'Ilarin',
	'title' => 'Ilarin',
	'mainUrl' => 'https://static-press-io.netlify.com/',
	'baseUrl' => 'https://static-press-io.netlify.com',
	'siteAuthor' => 'Ilarin',
	'collections' => [
		'posts' => [
			'path' => '{filename}',
			'sort' => '-date',
			'extends' => '_layouts.post',
			'section' => 'postContent',
			'isPost' => true,
			'comments' => false,
			'isFeatured' => true,
			'tags' => [],
			'categories' => [],
		],
		'tags' => [
			'path' => 'tag/{filename}',
			'extends' => '_layouts.tag',
			'section' => '',
			'name' => function ($page) {
				return $page->getFilename();
			},
		],
		'categories' => [
			'path' => '{filename}',
			'extends' => '_layouts.category',
			'section' => '',
			'name' => function ($page) {
				return $page->getFilename();
			},
		],
		'settings' => [
			'path' => 'settings/{filename}',
			'extends' => '_layouts.master',
			'section' => '',
			'name' => function ($page) {
				return $page->getFilename();
			},
		],
		'Pages' => [
			'path' => '{filename}',
			'extends' => '_layouts.page',
			'section' => '',
			'name' => function ($page) {
				return $page->getFilename();
			},
		],
	],
	// 'newnav' => file_get_contents('./source/navigation.json'),
	'excerpt' => function ($page, $limit = 250, $end = '...') {
		return $page->isPost
			? str_limit_soft(content_sanitize($page->getContent()), $limit, $end)
			: null;
	},
    'url' => function ($page, $path) {
        return starts_with($path, 'http') ? $path : '/' . trimPath($path);
    },
];